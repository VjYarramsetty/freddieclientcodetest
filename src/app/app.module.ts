import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { RowSubmitComponent } from './RowSubmit/row-submit.component';

@NgModule({
  declarations: [
    AppComponent, RowSubmitComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AgGridModule.withComponents([RowSubmitComponent])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
