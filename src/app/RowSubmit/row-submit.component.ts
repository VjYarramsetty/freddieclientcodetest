import { OnInit, Component } from "@angular/core";

@Component({
    selector: 'app-row-submit-component',
    templateUrl: './row-submit.component.html'
  })
export class RowSubmitComponent implements OnInit {
    data: any;
    params: any;
    constructor() {}
  
    agInit(params) {
      this.params = params;
      this.data = params.value;
    }
  
    ngOnInit() {}
  
    submitRow() {
      let rowData = this.params;
      let i = rowData.rowIndex;
      console.log(rowData);
      alert('Making a Post Call for Row: ' + rowData.data.name);
      // make a call Post call using httpclient module. 
      // We can have the necessary code written in a service class.
    }
  }
  