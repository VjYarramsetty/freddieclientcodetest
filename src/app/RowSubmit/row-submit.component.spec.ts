import { TestBed, async } from '@angular/core/testing';
import { RowSubmitComponent } from './row-submit.component';

describe('RowSubmitComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RowSubmitComponent
      ],
    }).compileComponents();
  }));

  it('should create the row submit component', () => {
    const fixture = TestBed.createComponent(RowSubmitComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
