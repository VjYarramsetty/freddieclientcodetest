import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AgGridAngular } from 'ag-grid-angular';
import { RowSubmitComponent } from './RowSubmit/row-submit.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private _jsonURL = 'assets/sample_data.json';
  rowData: any;

  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      console.log(data);
      this.rowData = data;
     });
  }

  @ViewChild('agGrid') agGrid: AgGridAngular;
  
  ngOnInit() {
    }

  columnDefs = [
    {headerName: 'name', field: 'name', sortable: true, filter: true, checkboxSelection: true},
		{headerName: 'phone', field: 'phone', sortable: true, filter: true},
		{headerName: 'email', field: 'email', sortable: true, filter: true},
		{headerName: 'company', field: 'company', sortable: true, filter: true},
		{headerName: 'date_entry', field: 'date_entry', sortable: true, filter: true},
		{headerName: 'org_num', field: 'org_num', sortable: true, filter: true},
		{headerName: 'address_1', field: 'address_1', sortable: true, filter: true},
		{headerName: 'city', field: 'city', sortable: true, filter: true},
		{headerName: 'zip', field: 'zip', sortable: true, filter: true},
		{headerName: 'geo', field: 'geo', sortable: true, filter: true},
		{headerName: 'pan', field: 'pan', sortable: true, filter: true},
		{headerName: 'pin', field: 'pin', sortable: true, filter: true},
		{headerName: 'id', field: 'id', sortable: true, filter: true},
		{headerName: 'status', field: 'status', sortable: true, filter: true},
		{headerName: 'fee', field: 'fee', sortable: true, filter: true},
		{headerName: 'guid', field: 'guid', sortable: true, filter: true},
		{headerName: 'date_exit', field: 'date_exit', sortable: true, filter: true},
		{headerName: 'date_first', field: 'date_first', sortable: true, filter: true},
		{headerName: 'date_recent', field: 'date_recent', sortable: true, filter: true},
    {headerName: 'url', field: 'url', sortable: true, filter: true},
    {headerName: 'Actions', field: '', cellRendererFramework: RowSubmitComponent }
  ];

  public getJSON(): Observable<any> {
    return this.http.get(this._jsonURL);
  }
}

